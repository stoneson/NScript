﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    public class IOHelper
    {
        public static String GetFullPath(String fromFilePath, String relativeFilePath)
        {
            string path;
            if (string.IsNullOrEmpty(fromFilePath))
            {

                path = System.IO.Path.GetFullPath(relativeFilePath);
                return path;
            }


            Uri fromUri = new Uri(fromFilePath);


            Uri toUri = new Uri(fromUri, relativeFilePath, true);

            //Uri relativeUri = fromUri.f(toUri);
            //String relativePath = Uri.UnescapeDataString(relativeUri.ToString());
            return toUri.AbsolutePath.Replace('/', Path.DirectorySeparatorChar);

        }

        public static string ReadAllText(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                throw new IOException("文件不存在:"+filePath);
            }
            return System.IO.File.ReadAllText(filePath);
        }

        public static void WriteAllText(string filePath,string content)
        {
            if (!System.IO.File.Exists(filePath))
            {
                throw new IOException("文件不存在:" + filePath);
            }
             System.IO.File.WriteAllText(filePath,content);
        }
    }
}
