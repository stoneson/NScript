﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    public class StringResources
    {
        public static string ExeHelp = @"
exe 说明
1) 可以用cmd命令运行本exe
   /run 命令格式:/run {filename} {args}    说明:必须实现Main入口函数. {filename} 为文件路径,{args}为Main入口参数，默认空格分隔。
   /help 命令格式:/help    说明:用户查看当前exe支持的命令说明。
2）脚本编辑器。
   可以用作.net 脚本的编辑工具，在实际运行环境中直接编辑或临时修改代码,开发环境中建议还是使用vs。
3）脚本运行时。
   整个脚本解析和运行时。
4) exe大小
   exe 本身很小很小，因为打包合并了第三方编辑器控件,才变更大些。

by 车江毅
";
        public static string ReferencedAssembliesDllsHelp = @"
帮助说明:
[系统编译默认添加的程序集]:系统默认添加到代码编译中支持的程序集,不用自己手工写dllfiles;
[dllfiles添加dll需要引用的程序集]:是开发者添加到主编译文件中，实际代码编译运行需要的dll支持。
[当前功能说明]:用于开发者方便自行排查一些dll未引用或者引用路径错误导致程序运行失败使用,系统检查结果仅供参考，以实际运行为准.
by 车江毅
";
        public static string AuthorInfo = @"
作者:车江毅
QQ群:.net 开源基础服务 238543768
描述:为了在.net平台下windows环境中实现类似java中groovy的脚本语言的相关功能和方向,希望它能够对你有帮助。";

        public static string CodeTemplate1 = @"

/*
 * codefiles=a.cs,codes\b.cs;//其他编译代码文件 ,分割多个文件 (支持相对路径) , 大小写敏感 （不要有分号和等号）
 * dllfiles=System.dll;//引用的dll,即编译需要的dll ,分割多个dll (支持相对路径) ,大小写敏感 （不要有分号和等号）
 * compilerlanguage=csharp;//编译语言类型,默认C#,可以不写
 */

/* 
* 以上为主文件的编译头信息,必须要写置顶在代码文件头部。 包含源码文件信息,dll相关引用信息,代码编写语言;
      头信息中不要非常规的去使用;和=号，这个是用来解析的分隔符。主文件建议使用.main.cs命名结尾,这样可以自动识别,其他代码文件为cs结尾。
* 以下为代码编码内容,语法遵循.net本身的语法及书写规范 
* by 车江毅
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mytest //可以不要命名空间也可
{
    public class B
    {
        //程序集或者应用程序域方式运行
        //关于调试: 通过“编辑器”->“调试” 暂不支持传入参数调试,但是实际环境是可以传入参数的
        public string test()
        {
            return new C().test();
        }

        //Main编译方式需要指定的Main入口函数
        //关于调试: 通过“编辑器”->“调试” 暂不支持传入参数调试,但是实际环境是可以传入参数的
        static void Main(string[] args)
        {
            System.Console.Read();
        }
    }
}

";
    }
}
