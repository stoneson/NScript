﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSF.BaseService.NScript
{
    public partial class FrInfo : Form
    {
        public FrInfo()
        {
            InitializeComponent();
        }

        public FrInfo(string msg, bool iscode=true)
        {
            InitializeComponent();
            if (iscode == true)
            {
                this.Text = "代码模板";
                txtContent.Document.HighlightingStrategy = HighlightingStrategyFactory.CreateHighlightingStrategy("C#");
                txtContent.Encoding = System.Text.Encoding.Default;
                this.txtContent.Text = msg;
            }
            else
            {
                this.Text = "提示信息";
                txtContent.ShowEOLMarkers = false;
                txtContent.ShowHRuler = false;
                txtContent.ShowInvalidLines = false;
                txtContent.ShowMatchingBracket = true;
                txtContent.ShowSpaces = false;
                txtContent.ShowTabs = false;
                txtContent.ShowVRuler = false;
                txtContent.AllowCaretBeyondEOL = false;
                txtContent.ShowLineNumbers = false;
                txtContent.Document.HighlightingStrategy = HighlightingStrategyFactory.CreateHighlightingStrategy("C#"); 
                txtContent.Encoding = System.Text.Encoding.Default;
                this.txtContent.Text = msg;
                this.btnCopy.Visible = false;
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(this.txtContent.Text); 
        }
    }
}
